package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			//configurando dados da tela
			primaryStage.setTitle("Login");
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
						
			//carregando arquivo da tela
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Login.fxml"));
			Parent arquivoXML = loader.load();
			
			//atribuindo arquivo XML e janela
			Scene emailLogin = new Scene(arquivoXML);
			primaryStage.setScene(emailLogin);
			
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
